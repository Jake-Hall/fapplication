-- phpMyAdmin SQL Dump
-- version 4.2.3deb1.trusty~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 15, 2015 at 07:16 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `Group`
--

CREATE TABLE IF NOT EXISTS `Group` (
`id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Group`
--

INSERT INTO `Group` (`id`, `name`) VALUES
(0, 'Guest / Not Logged In'),
(1, 'Banned User'),
(2, 'Registered User'),
(3, 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `Navigation`
--

CREATE TABLE IF NOT EXISTS `Navigation` (
`id` int(11) NOT NULL,
  `link` varchar(160) NOT NULL,
  `text` varchar(40) NOT NULL,
  `minGroupLevel` int(11) NOT NULL,
  `maxGroupLevel` int(11) NOT NULL,
  `class` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Navigation`
--

INSERT INTO `Navigation` (`id`, `link`, `text`, `minGroupLevel`, `maxGroupLevel`, `class`) VALUES
(1, '/', 'Home', 1, 3, 'left'),
(2, '/admin.php', 'Administration', 3, 3, 'right nav-red'),
(3, '/session.php?page=logout', 'Logout', 1, 3, 'right'),
(4, '/session.php?page=register', 'Register to Fapplication', 0, 0, 'left'),
(5, '/session.php?page=password-recovery', 'Password Recovery', 0, 0, 'right'),
(6, '/history.php', 'History', 2, 3, 'left'),
(7, '/session.php?page=login', 'Login', 0, 0, 'right');

-- --------------------------------------------------------

--
-- Table structure for table `Session`
--

CREATE TABLE IF NOT EXISTS `Session` (
  `session_id` varchar(100) NOT NULL DEFAULT '',
  `session_data` mediumtext NOT NULL,
  `session_expiry` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Transaction`
--

CREATE TABLE IF NOT EXISTS `Transaction` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `positivity` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TransactionType`
--

CREATE TABLE IF NOT EXISTS `TransactionType` (
`id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
`id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `group_id` int(3) NOT NULL,
  `password` varchar(120) NOT NULL,
  `ip` varchar(39) NOT NULL COMMENT 'Deprecated as this is now instead stored in Session',
  `hash` varchar(32) NOT NULL COMMENT 'Deprecated due to the use of phppassword_hash'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Group`
--
ALTER TABLE `Group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Navigation`
--
ALTER TABLE `Navigation`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Session`
--
ALTER TABLE `Session`
 ADD PRIMARY KEY (`session_id`), ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `Transaction`
--
ALTER TABLE `Transaction`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TransactionType`
--
ALTER TABLE `TransactionType`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Group`
--
ALTER TABLE `Group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `Navigation`
--
ALTER TABLE `Navigation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Transaction`
--
ALTER TABLE `Transaction`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TransactionType`
--
ALTER TABLE `TransactionType`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
