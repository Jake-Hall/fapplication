<?php
/**
 * Filename: 		appConfig.core.php
 * Last Updated:	10th April 2013
 *
 * Description:
 * ---------------------------------------------------------
 * This file contains code to pass the configuration for various
 * settings around the application, data should be edited in:
 * /cores/config.php. 
 */

class ApplicationConfig
{
    static $confArray;

    public static function read($name)
    {
        return self::$confArray[$name];
    }

    public static function write($name, $value)
    {
        self::$confArray[$name] = $value;
    }

}