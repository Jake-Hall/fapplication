<?php
/**
 * Handles authentication, logging in, registering and the validation behind 
 * each of those.
 */
 
if(!defined('APP_ACCESS')) {
	header( $_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
	exit;
}
 
class Authenticate {

    private static $instance;
    private $_db;
    
    public function __construct()
    {   
        // grab the current database instance and make it accessible within
        // the class
        $this->_db = DatabaseHelper::getInstance();
    }
    
    /**
     * Fetches the authenticating users details
     * @param string $emailAddress
     */
    public function fetchUserDetails( $emailAddress )
    {
        $this->_db->query("SELECT * FROM User "
                . "WHERE email = :email");
        
        $this->_db->bind(':email', $emailAddress);
        
        return $this->_db->resultset()[0];
    }
    
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}