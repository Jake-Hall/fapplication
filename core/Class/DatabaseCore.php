<?php
/**
 */
 
Class DatabaseCore
{
    public $dbh; // handle of the db connection
    private static $instance;

    private function __construct()
    {
        // Building the Database Connection with data read from the Config.php file
        $dsn = 'mysql:host=' . DatabaseConfig::read('DATABASE_HOST') .
               ';dbname='    . DatabaseConfig::read('DATABASE_NAME') .
               ';charset='      . DatabaseConfig::read('DATABASE_CHARSET') .
               ';connect_timeout=15';
        // getting DB user from config                
        $user = DatabaseConfig::read('DATABASE_USER');
        // getting DB password from config                
        $password = DatabaseConfig::read('DATABASE_PASSWORD');

     try {
	 
        $this->dbh = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    // other global functions
}

class DatabaseConfig
{
    static $confArray;

    public static function read($name)
    {
        return self::$confArray[$name];
    }

    public static function write($name, $value)
    {
        self::$confArray[$name] = $value;
    }

}

DatabaseConfig::write('DATABASE_HOST', DATABASE_HOST);
DatabaseConfig::write('DATABASE_NAME', DATABASE_NAME);
DatabaseConfig::write('DATABASE_CHARSET', DATABASE_CHARSET);
DatabaseConfig::write('DATABASE_USER', DATABASE_USER);
DatabaseConfig::write('DATABASE_PASSWORD', DATABASE_PASSWORD);