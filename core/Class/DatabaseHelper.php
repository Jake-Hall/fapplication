<?php
/**
 * Filename: 		DatabaseHelper.php
 * Last Updated:	February 2015
 *
 * Description:
 * ---------------------------------------------------------
 * Contains various methods that are useful and time-savers
 * 
 */
 
class DatabaseHelper {
	
    private $_db, $stmt;
    private static $instance;

    function __construct() {
        $this->_db = DatabaseCore::getInstance()->dbh;
    }
    
    /**
     * 
     * @param string $query Full query to be run
     */
    public function query( $query ) {
        $this->stmt = $this->_db->prepare($query);
    }
    
    /**
     * Here we are binding the values to the prepared statement, ready for input
     * into the db
     * @param string $param is the placeholder value that we will be using in 
     * our SQL statement, example :name.
     * @param string $value is the actual value that we want to bind to the 
     * placeholder, example “John Smith”.
     * @param string $type is the datatype of the parameter, example string.
     */
    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
              case is_int($value):
                $type = PDO::PARAM_INT;
                break;
              case is_bool($value):
                $type = PDO::PARAM_BOOL;
                break;
              case is_null($value):
                $type = PDO::PARAM_NULL;
                break;
              default:
                $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }
    
    /**
     * The execute method executes the prepared statement.
     * @return bool The result of the prepared statement execution
     */
    public function execute() {
        return $this->stmt->execute();
    }
    
    /**
     * The Result Set function returns an array of the result set rows
     * @return type array
     */
    public function resultset() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Single method simply returns a single record from the database.
     * @return array Array containing single result
     */
    public function single() {
        $this->execute();
        return $this->stmt->fetch()[0];
    }
    
    /**
     * returns the number of effected rows from the previous delete, update or 
     * insert statement. This method use the PDO method PDOStatement::rowCount.
     * @return int Count of rows affected by the previous statement
     */
    public function rowCount(){
        return $this->stmt->rowCount();
    }
    
    /**
     * 
     * @return string Last ID to be inserted
     */
    public function lastInsertId(){
        return $this->_db->lastInsertId();
    }
    
    public function beginTransaction(){
        return $this->_db->beginTransaction();
    }
    
    public function endTransaction(){
        return $this->_db->commit();
    }
    
    public function cancelTransaction(){
        return $this->_db->rollBack();
    }
    
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}