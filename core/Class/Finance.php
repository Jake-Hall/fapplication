<?php
class Finance {

    private $BalancePence;
    private $UserDetails;
    
/**
 * 
 * @param type $balanceA
 * @param type $balanceB
 */
    function __construct( $UserDetails ) {
        $this->BalancePence = $UserDetails["BalancePence"];
    }
    
   /**
    * 
    * @param type $value Value to be converted
    * @param type $round Deprecated
    * @return type
    */
    
    private function currencyConvert( $value, $round ) {

        if( is_int( $value ) )
        {
                return $value / 100;
        } else {
                return $value * 100;
        }

    }
    
    /**
     * 
     * @return string
     */
    
    public function userBalance() {
            return "&pound;" . $this->currencyConvert( $this->BalancePence, false );
    }

    public function userTransactions() {

            // Transactions for the user

    }
 
}