<?php
class Navigation {

    private $Nodes = [];
    private $User, $_db, $userLevel;
    
    function __construct() {
        $this->User = User::getInstance();
        $this->_db = DatabaseHelper::getInstance();
        $this->userLevel = $this->User->getUserPermissionLevel();
    }
    
    function buildNavHeader() {
        echo '<div id="navigation-container">';
    }
        
    /**
     * 
     * @param type $navigation
     * 
     * 
     * 
     *  array( 
     *      array( "link" => "",
     *      "text" => "",
     *      "authed" => "",
     *      "class" => "" ),
     * 
     *       
     */
   
    function buildNav() 
    {

        $this->_db->query("SELECT * FROM Navigation "
                . "WHERE minGroupLevel <= :level AND "
                . "maxGroupLevel >= :level");
        
        $this->_db->bind(':level', $this->userLevel);
        
        $navigation = $this->_db->resultset();
        
        foreach( $navigation as $key => $value ) {
           /*
            * If the user permission level (x) is more than or equal to the 
            * min level (y)
            * 
            * and the user permission level (x) is less than or equal to the 
            * max-group-level (z)
            * 
            * display the menu
            * 
            * if( x <= y && x >= z )
            */
            
            $userPermission = $this->User->getUserPermissionLevel();
            if( $value["minGroupLevel"] <= $userPermission
                && $value["maxGroupLevel"] >= $userPermission ) {                
                
                $this->Nodes[] = $this->buildNavLink($value["text"], 
                        $value["link"], 
                        $value["class"]);
            } else {
                continue;
            }
        }
        
        foreach( $this->Nodes as $key => $value ) {
            echo $value;
        }
    }
    
     /**
     * Builds navigation links passed.
     * 
     * @param string $text The title/text of the link
     * @param string $link The link that will be built
     * @param array $class Additional CSS to be applied to the nav link
     */
    function buildNavLink($text, $link, $class) {
        return "<a href=\"$link\">"
                . "<div class=\"item $class\" id=\"nav-"
                . strtolower($text) . "\">$text"
                . "</div>"
                . "</a>";
    }
    
    function buildNavFooter() {
        echo '<\div>';
    }
}