<?php

class PasswordUtility {
	
    public function hash( $password )
    {
            $options = array(
                    'cost' => 11
            );

            return password_hash( $password, PASSWORD_BCRYPT, $options);
    }

    public function verify( $DatabaseHash, $Hashed )
    {	
            return password_verify( $Hashed, $DatabaseHash );
    }
}
