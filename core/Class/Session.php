<?php
/**
 * Overwrites the inbuilt PHP Session handler to include our own, db driven
 * version. More control.
 */
 
if(!defined('APP_ACCESS')) {
	header( $_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
	exit;
}
 
class Session {

    protected $_sessionLifetime, $_db, $_dbHelper;
    private static $instance;

    public function __construct()
    {
        $this->_sessionLifetime = DatabaseConfig::read('SESSION_MAX_LIFETIME');
        $this->_db = DatabaseCore::getInstance()->dbh;
        
        $this->_dbHelper = DatabaseHelper::getInstance();

        $this->_LogSessionActivity( "N/A", "Constructor Called!" );
    }

    /**
     * isLoggedIn
     * 
     * Checks to see if the user you are authenticating is logged in.
     */

    public function isLoggedIn()
    {
        if(isset( $_SESSION['LoggedIn'] ) && $_SESSION['LoggedIn'] == true )
        {
            return true;
        }
        return false;
    }
    
    public function newSession( $save_path, $session_name ) {

        $sess_save_path = $save_path;

        $this->_LogSessionActivity( $session_name . " " . $save_path, "NewSession()" );
        return true;

}

    public function readSession( $id ) {

        $sth = $this->_db->prepare("SELECT * FROM `Session` WHERE `session_id` = :session_id");
        $sth->execute(array(
                ':session_id'    => $id
        ));

        if( $row = $sth->fetch() )
        {
                return $row['session_data'];
        }
        
        $this->_LogSessionActivity( $id, "ReadSession()" );
        return false;
    }

    public function writeSession( $id, $data ) {

        $sth = $this->_db->prepare("INSERT INTO `Session` (`session_id`, `session_data`, `session_expiry`) VALUES( :id, :data, :expires ) ON DUPLICATE KEY UPDATE `session_data` = :data, `session_expiry` = :expires");

        $end = $sth->execute(array(
                ':id'    => $id,
                ':data' => $data,
                ':expires' => time() + $this->_sessionLifetime
        ));

        if( !$end )
        {
                trigger_error("Database Error", E_USER_ERROR);
        }

        $this->_LogSessionActivity( $id, "WriteSession()" );
        return true;

    }

    public function destroySession( $id ) {

        $params = session_get_cookie_params();
        setcookie(session_name("SESSION_GEEK_PANEL"), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
        );

        $sth = $this->_db->prepare("DELETE FROM `Session` WHERE `session_id` = :id");
        $sth->execute(array(
                ':id' => $id
        ));

        $this->_LogSessionActivity( "Session ID - " . $id . " :: PDO Returned " . $sth->rowCount() . " rows deleted", "DestroySession()" );
        return true;
    }

    public function cleanSession() {

        $sth = $this->_db->prepare("DELETE FROM `Session` WHERE `expiry` < :time");
        $sth->execute(array(
                ':time' => time()
        ));

        $this->_LogSessionActivity( "N/A", "CleanSession()" );
        return true;

    }

    public function closeSession() {

        $this->_LogSessionActivity( "N/A", "CloseSession()" );
        return true;

    }

    /**
     * Used to overwrite existing session data, and create the authenticated
     * user a full session.
     * 
     * @param string $id Session Identifier, each user has a unique ID 
     * regardless of whether or not they are registered
     * @param string $userAgent Full user agent should be input like this
     * @param string $userInfo All of the details required for the session
     * should be stored within this string
     * @return boolean
     */

    public function createSessionData( $id, $userInfo )
    {		
        $_SESSION["ID"] = $id;
        $_SESSION["LoggedIn"] = true;
        $_SESSION["UserAgent"] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION["UserInfo"] = $userInfo;

        return true;
    }

    /**
     * Interally used within the Session class to log all session work made
     * through this handler
     *  
     * @param string $data Any data useful for debugging to be logged, 
     * e.g display the UUID from the session - or dump session data
     * @param type $type The type of session data being logged, this is 
     * just appended next to the data variable
     */

    private function _logSessionActivity( $data, $type, $enabled=true ) 
    {
        // Literally a temporary function to find out what
        // the hell is happening with the sessions. This logs
        // Every single time any of these functions are called
        if( $enabled == true )
        {
                $file = __DIR__ . '/SessionLog.txt';
                $current = file_get_contents($file);
                $current .= date("d/m/y H:i:s : ") . $type . ": " . $data . "\n";
                file_put_contents($file, $current);
        }
    }
    
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}

DatabaseConfig::write('SESSION_MAX_LIFETIME', MAX_SESSION_LIFE);
DatabaseConfig::write('COOKIE_DOMAIN', COOKIE_DOMAIN);

$Session = new Session();

session_set_save_handler( 
	array( &$Session, "NewSession" ), 
	array( &$Session, "CloseSession" ),
	array( &$Session, "ReadSession" ),
	array( &$Session, "WriteSession"),
	array( &$Session, "DestroySession"),
	array( &$Session, "CleanSession" )
);

session_set_cookie_params(0, '/', DatabaseConfig::read('COOKIE_DOMAIN'));
session_name("SESSION_GEEK_PANEL"); 
session_start();

$_SESSION['LoggedIn'] = $Session->IsLoggedIn();