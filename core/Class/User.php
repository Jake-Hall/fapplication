<?php
/**
 * Filename: 		User.php
 * Last Updated:	February 2015
 *
 * Description:
 * ---------------------------------------------------------
 * This class contains helpful user functions that are used 
 * around the panel, such as ways of fetching user data and
 * validating the users input
 */
 
class User {
    
    private static $instance;
    private $_db;
    
    function __construct()
    {
        $this->_db = DatabaseHelper::getInstance();
    }

    /**
     * Method that currently outputs the error message. Initially a method
     * for future design idea(s)
     * @param string $error
     * @param string $type
     * @return string Error Message
     */
    
    function throw_error( $error, $type ) {
        return $error;
    }
    
    public function isPagePublic() {
        if( !$_SESSION["LoggedIn"] ) {
            
            $publicPages = array(
                "/session.php?page=login",
                "/session.php?page=process-login",
            );
            
            if( in_array( $_SERVER["REQUEST_URI"], $publicPages) ) {
                return true;
            }

            return false;
        }
        
        // If user is logged in, return true
        return true;
    }
    
    public function emailExists( $emailAddress ) {
        $this->_db->query("SELECT id FROM User WHERE email = :email");
        $this->_db->bind(':email', $emailAddress);
        $this->_db->single();
        
        echo $this->_db->rowCount() . $emailAddress;
        if( $this->_db->rowCount() )
        {
            return true;
        } 
        
        return false;
    }
    
    /**
     * Retieves the currently logged in users permission level
     */
    public function getUserPermissionLevel()
    {
        if( $this->isLoggedIn() )
        {
            $this->_db->query("SELECT group_id FROM User "
                . "WHERE id = :id");
            $this->_db->bind(':id', $_SESSION['ID']);
            
            return $this->_db->single();
        }
        
        else return 0;
    }
    
    /**
     * isLoggedIn
     * 
     * Checks to see if the user you are authenticating is logged in.
     */
    
    public function isLoggedIn()
    {
        if(isset( $_SESSION['LoggedIn'] ) && $_SESSION['LoggedIn'] == true )
        {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param string $emailAddress Fetches the authenticating users details
     * from the database by Email Address. If the email doesn't exist, it will
     * return false
     * 
     * @return array User Details are returned in array format
     */
    private function fetchUserDetails( $emailAddress )
    {
        
    }
    
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}