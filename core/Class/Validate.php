<?php
class Validate {
    
    public function __construct() {
        // do nothing at the moment.
    }
    
    /**
     * Used to validate keys delivered through a post array form,
     * to validate whether they are legal fields, and to strip unneccesary
     * junk
     * 
     * submit is always allowed.
     * 
     * @param array $postArray Keys to be checked
     * @param type $allowedFields Keys that are legal and allowed
     * @return array 2D array, containing status and validated fields
     */
    public function arrayKeys($postArray, $allowedFields) {

        $clearedArray = [];
        $valid = true;
        $allowedFields[] = "submit";

        if(is_array($allowedFields) && is_array($postArray))
        {
            foreach( $postArray as $key => $value )
            {
                if( !in_array($key, $allowedFields))
                {
                    $valid = false;
                    break;
                }                    
                $clearedArray[$key] = $this->stripJunk($value);
            }
            return array("status" => $valid, "data" => $clearedArray);
        }
        // Either the $postArray or $allowedFields was not an array.
        return false;
    }
    
    /** 
     * @param string $content content to be stripped
     * @return string content stripped of everything but alphanumeric chars
     */
    
    function stripJunk( $content ) {
        return strip_tags( preg_replace("/[^a-z0-9_-\s.@,]+/i", "", $content) );
    }
    
   /**
     * Validates emails using the FILTER_VAR php function. Put into a method
     * to avoid complications down the line when changing.
     * 
     * @param type $emailAddress The email address to be checked
     * @return boolean 
     */
    public function emailAddress( $emailAddress ) {
        if(filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }
 
}