<?php
/**
 * Filename: 		Config.example.php
 *
 * Description:
 * -----------------------------------------------------------------------------
 *
 * Application Settings and configuration options can be set within the
 * config file. Please rename the file to "config.php" once you are happy
 * with the configuration
 * 
 * 
 * Enter than values into the DEFINE function, e.g:
 * 
 * DEFINE('DATABASE_HOST', 'localhost');
 */

/**
 *                      1. DATABASE SETTINGS
 * -----------------------------------------------------------------------------
 * These settings are an integral part of the application and must be correct
 * in order for it to function. If you are having any issues with the database,
 * you can be sure there is a problem with the configuration selected below.
 * -----------------------------------------------------------------------------
 */

/**
 * [DATABASE_HOST]
 *  The host the database server resides on, usually localhost.
 */
    DEFINE('DATABASE_HOST', '');
/**
 * [DATABASE_NAME]
 *  The name of the database data will be saved on, this will need to be set up
 *  within your hosting control panel, or by your webhost.
 */
    DEFINE('DATABASE_NAME', '');
/**   
 * [DATABASE_USER]
 *  The user that will be used to connect to the database. This user should have
 *  all permissions granted to the database entered above
 */
    DEFINE('DATABASE_USER', '');
/**
 * [DATABASE_PASSWORD]
 *  The password for the user selected above. 
 */
    DEFINE('DATABASE_PASSWORD', '');
/**
 * [DATABASE_CHARSET]
 *  Character set for the database, set to utf8 if unsure
 */
    DEFINE('DATABASE_CHARSET', '');


DEFINE('MAX_SESSION_LIFE', 1440); 
DEFINE('COOKIE_DOMAIN', '.example.com');