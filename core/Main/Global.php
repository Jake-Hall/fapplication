<?php
/**
 * Filename: 		Global.php
 * Last Updated:	February 2015
 *
 * Description:
 * ---------------------------------------------------------
 * This file imports all of the important classes and PHP
 * files that are used within this software.
 *
 *
 *
 * This whole file and its flow needs to be moved into a class. Yuck!
 */

    // disable direct access to files
    DEFINE('APP_ACCESS', true);

    $loadTestOneStart = microtime(TRUE);

    /**
     * Essentially runs through our files and loads them in. Should prob
     * use the functionality provided by php but this works. I want to
     * plonk this in a class at some point, however.
     */

    $FilesToLoad = array(
        "Main" => array(
            "Config"
        ),
        "Class" => array(
            "ApplicationConfig", 
            "DatabaseCore", 
            "DatabaseHelper", 
            "Session",
            "Authenticate",
            "User",
            "Password",
            "Finance",
            "Validate",
            "Navigation"
        ),
        "Helpers" => array(
            "HTMLPurifier.auto"
        ),
    );

    $FileStructure = array(
        "Class" => array(
            "Extension" => ".php",
            "Prepend" 	=> "",
            "Append"	=> "",
            "Location"	=> "core/Class/"
        ),
        "Main" => array(
            "Extension" => ".php",
            "Prepend" 	=> "",
            "Append"	=> "",
            "Location"	=> "core/Main/"
        ),
        "Helpers" => array(
            "Extension" => ".php",
            "Prepend"   => "",
            "Append"    => "",
            "Location"  => "core/Helpers/"
        )
    );

    // Public Pages that don't require the user to be autheticated
    $publicPages = array("session.php");

    // Runs through two loops, one grabs the files to be opened whilst 
    // the other opens the files.

    foreach( $FilesToLoad as $FileType => $FileArray )
    {
        foreach( $FileArray as $FileProperty => $FileValue )
        {
            require ( $FileStructure[$FileType]["Location"] 
                    . $FileStructure[$FileType]["Prepend"] 
                    . $FileValue 
                    . $FileStructure[$FileType]["Append"] 
                    . $FileStructure[$FileType]["Extension"] 
                );
        }
    }	

    /**
     * HTML Purifier intialization. 
     * 
     * http://htmlpurifier.org/live/INSTALL
     */

    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    $PasswordUtility = new PasswordUtility();

    $User = new User();
    $Authenticate = new Authenticate();

    //$database = DatabaseCore::getInstance();
    $DatabaseHelper = new DatabaseHelper();
    $Validate = new Validate();
    
    $Navigation = new Navigation();