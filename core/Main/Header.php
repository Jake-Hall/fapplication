<?PHP
    if(!defined('HEADER_ACCESS')) {
	trigger_error("Do not directly invoke Header.php - Call core/Structure/Header.php", E_USER_ERROR);
	exit;
    }
    
    require("Global.php");
    
    // Run the IsPagePublic method to check to see if the session is authenticated.
    // If not, we then check to see if the session is accessing a publically
    // available URL
    
    if( !$User->IsPagePublic() )
    {
        Header("Location: session.php?page=login");
        exit;
    }
?>