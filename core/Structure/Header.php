<?php

/**
 * Filename: Header.php
 * ---------------------
 * 
 * Used as the header for every single pageview within the app,
 * contains session management.
 */

DEFINE('HEADER_ACCESS', true);

// let's begin by calling our main file, which contains session management,
// db connection and class intialization required to work
require("core/Main/Header.php");

/* 
 * If we have reached this point, it means our user is logged in. Let's grab
 * their details and build up an array to use for the app (to be implemented)
 */

$appSettings = array("BalancePence" => 240082, "BalanceUser" => "Jake");
$Finance = new Finance( $appSettings );

?>

<!DOCTYPE html>
<!--
    Website: finance.thegeek.info
    Author: Jake Hall - jake@thegeek.info

    The website makes use of Unsemantic, the fantastic, fluid and easy to use
    grid system, which is available over at http://unsemantic.com
-->
<html>
    <head>
        <title>myFinance :: financial stuff</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />
        
        <!-- 
            Load up required CSS Files
        
            1. grid.css
            2. style.css
        -->
        
        <link rel="stylesheet" href="static/css/responsive-grid.css">
	<link rel="stylesheet" href="static/css/style.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="html/js/main.js"></script>
    </head>
    
    <body>
        <div id="header">
            <div class="grid-container">
                <div id="logo" class="grid-65"><strong>my</strong>finance</div>
                <div id="navigation-menu" class="grid-35">
                    <?PHP         
                        $Navigation->buildNav();
                    ?>
                </div>
            </div>
        </div>

            <div id="grid-container" class="grid-container">
