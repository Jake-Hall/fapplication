/* 
*
*  Javascript File containing Javascript Functions
*  for the various forms on the script.
*
*  Makes use of the javascript library jQuery
*
*
*	THIS CODE IS A MESS. IT IS NOT FINAL. IT NEEDS TO BE REVIEWED.
*
*/

// Waits for the document to be ready, then prepares the javascript

$(document).ready(function () {

	// Declaring some variables
	
	var pathname;
	var file;
	var menu = [];
	var parentMenu = ["home", "tickets", "downloads", "account", "sessions"];
	var iterator;

	$('div[name="clearForm"]').click(function() {
		$( "#dialog-confirm" ).dialog({
      resizable: false,
      height:172,
      modal: true,
      buttons: {
        "Reset Form": function() {
		$('textarea[name="contents"]').val("");
		$('input[name="subject"]').val("");
          $( this ).dialog( "close" );
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
	});
	
	// Navigation Selector... there may be a better way to do this but I thought JavaScript was the most 
	// streamlined way instead of relying on PHP. Can definitely be looked into at a later date...
	
	function getFilename(f) {
		return (( f == "" || f == "/" ) ? "home" : f.replace(/^.*[\\\/]/, '').slice(0, -4) );
	}
	
	function getParentMenu(path) {
		/*
		* Find out where the page belongs
		* 0 = home. 1 = ticket. 2 = downloads. 3 = account and 4 = session stuff
		*
		*/
		
		var menuIterator;
		
		menu = [
			[ "home", "index" ], 
			[ "tickets", "viewticket", "newticket" ], 
			[ "downloads" ], 
			[ "account" ], 
			[ "sessions", "debug", "session" ] 
		];
		
		for (var i = 0; i < menu.length; i++) {
			for (var h = 0; h < menu[i].length; h++) {
				if (menu[i][h] == path)
				{
					menuIterator = i;
					break;
				}
			}
		}
		
		$("#nav-" + parentMenu[menuIterator]).addClass("selected");
	}
	
	pathname = getFilename( window.location.pathname );
	getParentMenu( pathname );
});