<?php

/**
 * Filename: index.php
 * ---------------------
 * 
 * The main, default page for the app.
 */

    // Let's grab our header of the structure
    require("core/Structure/Header.php");
?>
                <div class="grid-60 mobile-grid-60">
                    <div class="grid-item">
                        
                        <h2>finance dashboard</h2>                        
                        <p>
                            Hello there, <strong><?php echo $_SESSION['UserInfo']['FirstName'] . " " . $_SESSION['UserInfo']['LastName']; ?></strong>. Welcome to the myfinance dashboard.
                        </p>
                    </div>
                </div>
                 <div class="grid-40 mobile-grid-40">
                    <div class="grid-item">
                        <h2>at a glance</h2>
                        <p>
                            whats going on with my account sir?
                        </p>
                    </div>
                 </div>
                <div class="grid-100 mobile-grid-100">
                    <div class="grid-item">
                        <h2>finance is cool</h2>
                        <p>
                            lets actually get this going
                        </p>
                    </div>
                </div>
            </div>
    </body>
</html>
