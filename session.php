<?php

/**
 * FLAG: CODE VET REQUIRED.
 * 
 * This file needs to be updated and optimized. 
 */
include("core/Structure/Header.php");

/*
 * Define the "page" variable, this will check to see if the session is on
 * one of our other pages, such as logout or register. 
 * 
 * If there is no page set, we assume the user wants to login. If they are 
 * already logged in, they will be redirected before this point anyway.
 */

$page = ( ( isset( $_GET["page"]) ? $_GET["page"] : "login") );

?>
                <div class="grid-80 mobile-grid-100 grid-center">
                    <div class="grid-item">
                        
                        <h2>login to myfinance</h2>                        
                        <?php
// Display the login page if the user is attempting to login
if( $page == "login") {
    
    require("core/Main/LoginForm.php");    
}
else if( $page == "process-login" && !$Session->isLoggedIn() ) {
    if( isset($_POST["submit"]) )
    {
        $validatedData = $Validate->arrayKeys($_POST, 
                                            ["email_address", "password"]);

        if( $validatedData["status"] == false ) {
            echo "Illegal POST Value";
            var_dump($validatedData);
        } else {
            
            $postData = $validatedData["data"];
            
            // Let's start with checking the email address
            if( !$User->emailExists($postData["email_address"]) )
            {
                $errors[] = "The email address entered is not "
                        . "one that we recognize";
            } else {
                
                $UserDetails = $Authenticate->fetchUserDetails($postData["email_address"]);
                
                if( !$PasswordUtility->verify(
                        $UserDetails["password"], $postData["password"]) ) {
                    
                    $errors[] = "The password entered does not "
                        . "match the one we have on file";
                }
               
            }
            
            // Check to see if any errors exist within the array
            if( count($errors) !== 0 ) {
                foreach( $errors as $error ) {
                    echo $error;
                }
                
                // redirect back to the login page
            } else {
                           
                $userInfo = array(
                    "FirstName" => $UserDetails["first_name"],
                    "LastName" => $UserDetails["last_name"]
                );

                $Session->createSessionData( $UserDetails["id"], $userInfo );
                header("Location: /index.php");
            }
        }
    } else {
        // The form was not submitted, move the user away
        header("Location: /session.php?page=login");
    }
} else if( $page == "logout") {
    session_destroy();
    Header("Location: /");
} 
// The user has attempted to navigate to a page we don't expect, so lets send 
// them away to the index page
else {
    header("Location: /index.php");
}

?>
                    </div>
                </div>
            </div>
    </body>
</html>